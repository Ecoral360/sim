#### TODO SIMUL

1. preanimer une animation
    1. sauvegarder l'état d'une animation
    2. charger l'état d'une animation

2. moteur physique
    1. Object statique (impossible à bouger)
    2. Objet dynamique (sensible aux forces appliquées sur lui)