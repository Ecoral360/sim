package org.simul.console;

import java.util.Scanner;

public class Console {
    public static void writeLine(String separator, Object... objects) {
        for (var obj : objects)
            System.out.print(obj + separator);
        System.out.println();
    }

    public static void writeLine(Object... objects) {
        writeLine(" ", objects);
    }

    private static Scanner reader(String message) {
        System.out.print(message);
        return new Scanner(System.in);
    }

    public static int readInt(String message) {
        return reader(message).nextInt();
    }

    public static int readInt() {
        return reader("").nextInt();
    }

    public static double readDouble(String message) {
        return reader(message).nextInt();
    }

    public static double readDouble() {
        return reader("").nextDouble();
    }

    public static String readLine(String message) {
        return reader(message).nextLine();
    }

    public static String readLine() {
        return reader("").nextLine();
    }

}
