package org.simul.exemples.animation.timeline;

import org.simul.swing.animation.Timeline;
import org.simul.swing.animation.creation.Rotation;
import org.simul.swing.animation.creation.Scale;
import org.simul.swing.animation.creation.Translation;
import org.simul.swing.dessin.Dessinateur;

import javax.swing.*;
import java.awt.*;

public class ZoneAnimation extends JPanel {
    private final Timeline timeline;
    private final Etoile etoile1, etoile2;

    public ZoneAnimation() {
        setBackground(Color.black);
        setLayout(null);

        this.etoile1 = new Etoile(100, 100, 100, 50);
        this.etoile2 = new Etoile(100, 200, 50, 25);

        this.timeline = new Timeline(this, 60);
        this.timeline.ajouterAnimation(new Translation(etoile1, 100, 0, 2, false));
        this.timeline.ajouterAnimation(new Translation(etoile2, 40, -50, 1.4, false), Timeline.AVEC_PRECEDENT);
        this.timeline.ajouterAnimation(new Translation(etoile1, -20, 150, 3.1, false), Timeline.APRES_PRECEDENT);
        this.timeline.ajouterAnimation(new Rotation(etoile2, Math.toRadians(360), 1, false), Timeline.APRES_PRECEDENT);
        this.timeline.ajouterAnimation(new Scale(etoile2, 0.5, 0.1, false), Timeline.APRES_PRECEDENT);
    }


    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        Dessinateur dessinateur = new Dessinateur(g2d);

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        dessinateur.dessiner(etoile1);
        dessinateur.dessiner(etoile2);

        g2d.setColor(Color.WHITE);

        g2d.setFont(new Font("timesnewroman", Font.PLAIN, 20));
        g2d.drawString("Voici du texte!", 20, 40);

        g2d.setColor(Color.green);

    }

    //----------------- Getters && Setters -----------------//

    public Timeline getTimeline() {
        return timeline;
    }
}
