package org.simul.exemples.animation.timeline;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.io.Serial;

/**
 * Fenetre principale qui permet de tester differents types d'objets dessinables
 *
 * @author Caroline Houle
 */
public class Application extends JFrame {
    @Serial
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;

    /**
     * Constructeur de l'application
     */
    public Application() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 694, 507);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        ZoneAnimation animation = new ZoneAnimation();
        animation.setBounds(15, 11, 647, 370);
        contentPane.add(animation);
        animation.setLayout(null);

        JButton btnBouger = new JButton("Toggle l'animation!");
        btnBouger.setBounds(15, 403, 385, 41);
        btnBouger.addActionListener(e -> animation.getTimeline().toggle());
        contentPane.add(btnBouger);

    }//fin constructeur

    /**
     * D\u00E9marrage
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                Application frame = new Application();
                frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}//fin classe


