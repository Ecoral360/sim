package org.simul.exemples.animation.surfaceanime;

import org.simul.swing.animation.interfaces.SurfaceAnimable;
import org.simul.swing.dessin.Dessinateur;

import javax.swing.*;
import java.awt.*;
import java.io.Serial;

/**
 * Composant graphique qui cr\u00E9e et dessine divers objets dessinables
 *
 * @author Mathis Laroche
 */
public class ZoneAnimation extends JPanel implements SurfaceAnimable {
    @Serial
    private static final long serialVersionUID = -1L;
    private Etoile etoile1, etoile2;

    /**
     * Creation du composants et de son contenu
     */
    public ZoneAnimation() {
        setBackground(Color.black);
        setLayout(null);

        creerObjetsDessinables();
    } //fin du constructeur


    /**
     * Creation des divers objets dessinables (ceux qui ne d\u00E9pendent
     * ni de getWidth ni de getHeight)
     */
    private void creerObjetsDessinables() {
        etoile1 = new Etoile(100, 100, 100, 50);
        etoile2 = new Etoile(100, 200, 50, 25);
    }

    /**
     * Methode de dessin du composant (redefinition)
     *
     * @param g Le contexte graphique
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        Dessinateur dessinateur = new Dessinateur(g2d);

        //pour adoucir les contours
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2d.setColor(Color.WHITE);

        dessinateur.dessiner(etoile1);
        dessinateur.dessiner(etoile2);

        g2d.setFont(new Font("timesnewroman", Font.PLAIN, 20));
        g2d.drawString("Voici du texte!", 20, 40);

        g2d.setColor(Color.green);

    }//fin paintComponent


    @Override
    public boolean prochaineImage(double deltaT) {
        var dx = 5;
        var dy = 1;
        etoile1.setX(etoile1.getX() + dx);
        etoile2.setY(etoile2.getY() + dy);
        etoile1.creerGeometrie();
        etoile2.creerGeometrie();
        return etoile1.getX() + 30 < getWidth();
    }

    @Override
    public void reinitialiser() {
        creerObjetsDessinables();
    }
}//fin classe


















