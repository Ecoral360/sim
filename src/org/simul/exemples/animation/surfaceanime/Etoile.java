package org.simul.exemples.animation.surfaceanime;


import org.simul.swing.animation.creation.Geometrie;
import org.simul.swing.dessin.Dessinable;

import java.awt.*;
import java.awt.geom.Path2D;

/**
 * Classe qui repr\u00E9sente une \u00E9toile \u00e0 5 pointes, dessinee au trait.
 * On sp\u00E9cifie les rayons ext\u00E9rieurs et int\u00E9rieurs imaginaires sur lesquels les pointes
 * de l'\u00E9toile seront plac\u00E9es.
 *
 * @author Caroline Houle
 */
public class Etoile extends Geometrie implements Dessinable {
    /**
     * le path qui trace l'\u00E9toile \u00e0 cinq pointes
     */
    private Path2D.Double pathEtoile;
    /**
     * pointes ext\u00E9rieures dipos\u00E9es sur un cercle de ce rayon
     */
    private double rayonExterieur;
    /**
     * pointes int\u00E9rieures dispos\u00E9es sur un cercle de ce rayon
     */
    private double rayonInterieur;

    /**
     * Construire une \u00E9toile \u00e0 cinq pointes, en sp\u00E9cifiant les rayons ext\u00E9rieur et int\u00E9rieur imaginaires
     * sur lesquels les pointes seront plac\u00E9es.
     *
     * @param x              Le x du coin sup\u00E9rieur-gauche du rectangle englobant l'\u00E9toile
     * @param y              Le y du coin sup\u00E9rieur-gauche du rectangle englobant l'\u00E9toile
     * @param rayonExterieur Rayon du cercle sur lequel les pointes ext\u00E9rieures seront dispos\u00E9es
     * @param rayonInterieur Rayon du cercle sur lequel les pointes int\u00E9rieures seront dispos\u00E9es
     */
    public Etoile(double x, double y, double rayonExterieur, double rayonInterieur) {
        super(x, y);
        this.rayonExterieur = rayonExterieur;
        this.rayonInterieur = rayonInterieur;

        creerGeometrie();
    }

    /**
     * M\u00E9thode pour cr\u00E9er la forme de l'\u00E9toile ({@link Path2D})
     * Cette m\u00E9thode doit \u00EAtre appel\u00E9e de nouveau chaque fois que sa position ou dimension est modifi\u00E9e
     */
    @Override
    public void creerGeometrie() {
        final double x = getX(), y = getY();

        //avec ces angles, on crée une étoile à cinq pointes
        double angle1 = Math.PI / 10;
        double angle2 = Math.PI * 3 / 10.0;
        double ptX, ptY; //variables temporaires pour calculer les points à rejoindre entre eux

        pathEtoile = new Path2D.Double();

        ptX = x + rayonExterieur;
        ptY = y;
        pathEtoile.moveTo(ptX, ptY);

        ptX = x + rayonExterieur - rayonInterieur * Math.cos(angle2);
        ptY = y + rayonExterieur - rayonInterieur * Math.sin(angle2);
        pathEtoile.lineTo(ptX, ptY);

        ptX = x + rayonExterieur - rayonExterieur * Math.cos(angle1);
        ptY = y + rayonExterieur - rayonExterieur * Math.sin(angle1);
        pathEtoile.lineTo(ptX, ptY);

        ptX = x + rayonExterieur - rayonInterieur * Math.cos(angle1);
        ptY = y + rayonExterieur + rayonInterieur * Math.sin(angle1);
        pathEtoile.lineTo(ptX, ptY);

        ptX = x + rayonExterieur - rayonExterieur * Math.cos(angle2);
        ptY = y + rayonExterieur + rayonExterieur * Math.sin(angle2);
        pathEtoile.lineTo(ptX, ptY);

        ptX = x + rayonExterieur;
        ptY = y + rayonExterieur + rayonInterieur;
        pathEtoile.lineTo(ptX, ptY);

        ptX = x + rayonExterieur + rayonExterieur * Math.cos(angle2);
        ptY = y + rayonExterieur + rayonExterieur * Math.sin(angle2);
        pathEtoile.lineTo(ptX, ptY);

        ptX = x + rayonExterieur + rayonInterieur * Math.cos(angle1);
        ptY = y + rayonExterieur + rayonInterieur * Math.sin(angle1);
        pathEtoile.lineTo(ptX, ptY);

        ptX = x + rayonExterieur + rayonExterieur * Math.cos(angle1);
        ptY = y + rayonExterieur - rayonExterieur * Math.sin(angle1);
        pathEtoile.lineTo(ptX, ptY);

        ptX = x + rayonExterieur + rayonInterieur * Math.cos(angle2);
        ptY = y + rayonExterieur - rayonInterieur * Math.sin(angle2);
        pathEtoile.lineTo(ptX, ptY);

        pathEtoile.closePath();
    }

    /**
     * Dessiner l'\u00E9toile.
     * Cette m\u00E9thode doit garder le contexte graphique
     * g2d intacte, car possiblement d'autres
     * objets l'utiliseront par la suite.
     *
     * @param g2d Le contexte graphique du composant sur lequel on dessine
     */
    public void dessiner(Graphics2D g2d) {
        g2d.setColor(Color.YELLOW);
        g2d.rotate(getAngle(), pathEtoile.getBounds2D().getCenterX(), pathEtoile.getBounds2D().getCenterY());
        g2d.scale(getScaleX(), getScaleY());
        g2d.draw(pathEtoile);
    }


}// fin classe
