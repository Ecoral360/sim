package org.simul.exemples.prettyprint;

import org.simul.utils.annotations.PrettyPrint;

public class Personne {
    @PrettyPrint
    private final String name;

    @PrettyPrint
    private int age;

    private String email;

    public Personne(int age, String name, String email) {
        this.age = age;
        this.name = name;
        this.email = email;
    }

    //----------------- Getters && Setters -----------------//


    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}
