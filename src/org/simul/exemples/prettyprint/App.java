package org.simul.exemples.prettyprint;

import org.simul.utils.lang.prettyprint.PrettyPrinter;

public class App {
    public static void main(String[] args) {
        Personne personne = new Personne(18, "Mathis", "abc@gmail.com");
        PrettyPrinter.println(personne);
    }
}
