package org.simul.exemples.dessin;

import org.simul.swing.dessin.Dessinateur;

import javax.swing.*;
import java.awt.*;
import java.io.Serial;

import static org.simul.swing.event.ComponentEvent.runOnceAdded;

/**
 * Composant graphique qui cr\u00E9e et dessine divers objets dessinables
 *
 * @author Mathis Laroche
 */
public class Scene extends JPanel {
    @Serial
    private static final long serialVersionUID = -1L;
    private boolean dessinerAvecDessinateur = true;
    private Etoile etoile;

    /**
     * Creation du composants et de son contenu
     */
    public Scene() {
        setBackground(Color.black);
        setLayout(null);
        creerObjetsDessinables();

        runOnceAdded(this, () -> {

        });
    } //fin du constructeur


    /**
     * Creation des divers objets dessinables (ceux qui ne d\u00E9pendent
     * ni de getWidth ni de getHeight)
     */
    private void creerObjetsDessinables() {
        etoile = new Etoile(100, 100, 100, 50);
    }

    public boolean isDessinerAvecDessinateur() {
        return dessinerAvecDessinateur;
    }

    public void setDessinerAvecDessinateur(boolean dessinerAvecDessinateur) {
        this.dessinerAvecDessinateur = dessinerAvecDessinateur;
        repaint();
    }

    /**
     * Methode de dessin du composant (redefinition)
     *
     * @param g Le contexte graphique
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        //pour adoucir les contours
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2d.setColor(Color.WHITE);
        if (dessinerAvecDessinateur) {
            /* les changements fait a l'interieur de la methode dessiner de l'etoile ne sont pas leak */
            Dessinateur dessinateur = new Dessinateur(g2d);
            dessinateur.dessiner(etoile);
        } else {
            /* le changement de couleur fait a l'interieur de la methode dessiner de l'etoile
             * leak et impacte la couleur du texte
             */
            etoile.dessiner(g2d);
        }

        g2d.setFont(new Font("timesnewroman", Font.PLAIN, 20));
        g2d.drawString("Voici du texte!", 20, 40);

        g2d.setColor(Color.green);

    }//fin paintComponent


}//fin classe