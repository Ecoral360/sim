package org.simul.math.exceptions;

public class VecteurNormalImpossible extends RuntimeException {
    public VecteurNormalImpossible() {
        super();
    }

    public VecteurNormalImpossible(String message) {
        super(message);
    }
}
