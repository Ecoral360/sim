package org.simul.swing.animation;

import org.simul.swing.animation.enums.EtatAnimation;
import org.simul.swing.animation.enums.Timing;
import org.simul.swing.animation.interfaces.Animable;
import org.simul.swing.animation.interfaces.SurfaceAnimable;

import javax.swing.*;
import java.util.*;
import java.util.function.Predicate;

/**
 * Classe permettant de cr\u00E9er une timeline d'animations qui vont s'ex\u00E9cuter dans
 * l'ordre d\u00E9sir\u00E9
 *
 * @author Mathis Laroche
 */
public class Timeline implements Animable {
    public static final Timing AVEC_PRECEDENT = Timing.AVEC_PRECEDENT;
    public static final Timing APRES_PRECEDENT = Timing.APRES_PRECEDENT;

    private final LinkedList<Hashtable<Animable, Boolean>> timeline = new LinkedList<>();
    private final Animateur animateur;

    private double tempsActuel = 0;

    /**
     * Constructeur responsable de cr\u00E9er une nouvelle Timeline
     *
     * @param surface La surface sur laquelle les animations auront lieux
     * @param sleep   Le temps entre les frames
     *                (le temps attendu apr\u00E8s chaque appel de la fonction
     *                {@link #prochaineImage(double) prochaineImage(intervalle)})
     * @param deltaT  q
     */
    public Timeline(JPanel surface, int sleep, double deltaT) {
        this.animateur = new Animateur(SurfaceAnimable.from(surface, this), sleep, deltaT);
    }

    /**
     * Constructeur responsable de cr\u00E9er une nouvelle Timeline
     *
     * @param surface La surface sur laquelle les animations auront lieux
     * @param sleep   Le temps entre les frames
     *                (le temps attendu apr\u00E8s chaque appel de la fonction
     *                {@link #prochaineImage(double) prochaineImage(intervalle)})
     */
    public Timeline(JPanel surface, int sleep) {
        this.animateur = new Animateur(SurfaceAnimable.from(surface, this), sleep);
    }

    /**
     * Constructeur responsable de cr\u00E9er une nouvelle Timeline
     *
     * @param surface La surface sur laquelle les animations auront lieux
     * @param deltaT  q
     */
    public Timeline(JPanel surface, double deltaT) {
        this.animateur = new Animateur(SurfaceAnimable.from(surface, this), deltaT);
    }

    /**
     * Ajoute une animation \u00e0 dans la Timeline qui sera effectu\u00E9e selon le
     * {@link Timing timing} pr\u00E9cis\u00E9 en param\u00E8tre
     * <br>
     * Si le timing est {@link Timing#AVEC_PRECEDENT} et que l'animation est le premier \u00E9l\u00E9ment de
     * la timeline, aucune erreur n'est lanc\u00E9e et l'animation est ajout\u00E9e au d\u00E9but de la timeline
     *
     * @param animation l'animation {@link Animable} \u00e0 ajouter \u00e0 la timeline
     * @param timing    indique si \u00e0 quel moment l'animation devrait \u00EAtre jou\u00E9 par rapport
     *                  \u00e0 l'animation pr\u00E9c\u00E9dente
     */
    public void ajouterAnimation(Animable animation, Timing timing) {
        if (timing == Timing.AVEC_PRECEDENT && timeline.isEmpty())
            timing = Timing.APRES_PRECEDENT;

        switch (timing) {
            case AVEC_PRECEDENT -> timeline.getLast().put(animation, true);
            case APRES_PRECEDENT -> timeline.addLast(new Hashtable<>(Map.of(animation, true)));
        }
    }

    /**
     * Ajoute une animation \u00e0 dans la Timeline avec le timing {@link Timing#APRES_PRECEDENT}
     *
     * @param animation l'animation {@link Animable} \u00e0 ajouter \u00e0 la timeline
     */
    public void ajouterAnimation(Animable animation) {
        ajouterAnimation(animation, Timing.APRES_PRECEDENT);
    }

    /**
     * Ajoute un groupe ({@link List List&lt;Animation&gt;}) d'animation \u00e0 la timeline.<br>
     * La premi\u00E8re animation du groupe est ajout\u00E9e selon le timing pass\u00E9 en param\u00E8tre
     * puis les autres sont ajout\u00E9es avec le timing {@link Timing#AVEC_PRECEDENT}
     *
     * @param animations le groupe d'animations \u00e0 ajouter
     * @param timing     le timing du premier \u00E9l\u00E9ment de la liste d'animation
     */
    public void ajouterGroupeAnimations(List<Animable> animations, Timing timing) {
        if (animations.isEmpty()) return;
        ajouterAnimation(animations.get(0), timing);
        animations
                .subList(1, animations.size())
                .forEach(animation -> ajouterAnimation(animation, Timing.AVEC_PRECEDENT));
    }

    /**
     * Enl\u00E8ve le groupe d'animation, \u00e0 l'index indiqu\u00E9 en param\u00E8tre, de la timeline
     *
     * @param idx l'index du groupe \u00e0 retirer
     */
    public void retirerGroupeAnimations(int idx) {
        timeline.remove(idx);
    }

    /**
     * Enl\u00E8ve toutes les instances de l'animation pass\u00E9e en param\u00E8tre pr\u00E9sente dans la timeline
     *
     * @param animation l'animation \u00e0 retirer de la timeline
     */
    public void retirerAnimation(Animable animation) {
        timeline.forEach(animations -> animations.remove(animation));
    }

    /**
     * M\u00E9thode \u00E9quivalente \u00e0 {@link #getAnimateur()}.{@link Animateur#toggle() toggle()}
     *
     * @return le nouvel \u00E9tat de l'animation
     */
    public EtatAnimation toggle() {
        return animateur.toggle();
    }

    /**
     * @param deltaT Interval de temps entre cette frame et la frame precedante (en secondes)<br>
     *               <i><b>ATTENTION</b></i>: L'exactitude de cette valeur n'est <i>PAS</i> garantie!
     * @return <code>true</code> si l'animation n'est pas finie et <code>false</code> si elle est finie
     */
    @Override
    public boolean prochaineImage(double deltaT) {
        tempsActuel += deltaT;

        /* get les animations a jouer */
        var currentAnimation = getNextAnimation().orElse(null);

        /* si toutes les animations ont fini de jouer */
        if (currentAnimation == null) return false;

        /* jouer la prochaine image des animations a jouer */
        currentAnimation.replaceAll((animable, estPasFini) -> estPasFini && animable.prochaineImage(deltaT));
        return true;
    }

    private Optional<Hashtable<Animable, Boolean>> getNextAnimation() {
        return timeline.stream()
                .dropWhile(animations -> animations.values()
                        .stream()
                        .allMatch(Predicate.isEqual(false)))
                .findFirst();
    }

    /**
     * Enl\u00E8ve toutes les animations dans la timeline
     */
    public void effacer() {
        timeline.clear();
    }

    @Override
    public void reinitialiser() {
        timeline.forEach(animations -> animations.replaceAll((animable, estPasFini) -> {
            animable.reinitialiser();
            return true;
        }));
        tempsActuel = 0;
    }

    //----------------- Getters && Setters -----------------//

    /**
     * Retourne le temps o\u00F9 la timeline n'a pas \u00E9t\u00E9 en pause depuis le d\u00E9but
     * de l'animation<br>
     * Cette valeur est remise \u00e0 z\u00E9ro par la m\u00E9thode {@link #reinitialiser()}
     *
     * @return le temps o\u00F9 la timeline n'a pas \u00E9t\u00E9 en pause depuis le d\u00E9but
     * de l'animation
     */
    public double getTempsActuel() {
        return tempsActuel;
    }

    /**
     * Retourne l'objet {@link Animateur animateur} responsable de g\u00E9rer l'\u00E9tat de la timeline
     *
     * @return l'objet {@link Animateur animateur} responsable de g\u00E9rer l'\u00E9tat de la timeline
     */
    public Animateur getAnimateur() {
        return animateur;
    }
}
