package org.simul.swing.animation.interfaces;

import org.simul.swing.animation.Animateur;

import java.util.Map;

/**
 * Interface permettant d'utiliser l'{@link Animateur} pour rendre le processus
 * d'animation plus simple
 *
 * @author Mathis Laroche
 */
public interface Animable {
    /**
     * @param deltaT Interval de temps entre cette frame et la frame precedante (en secondes)<br>
     *               <i><b>ATTENTION</b></i>: L'exactitude de cette valeur n'est <i>PAS</i> garantie!
     * @return <code>true</code> si l'animation n'est pas finie et <code>false</code> si elle est finie
     */
    boolean prochaineImage(double deltaT);

    /**
     * Remet l'animation dans son etat initial
     */
    void reinitialiser();

    default Map<String, Object> enregistrerEtat() {
        return null;
    }

    default void chargerEtat(Map<String, Object> etat) {

    }
}
