package org.simul.swing.animation.interfaces;

import javax.swing.*;

/**
 * @author Mathis Laroche
 */
public interface SurfaceAnimable extends Animable {

    static SurfaceAnimable from(JPanel panel, Animable animable) {
        return new SurfaceAnimable() {
            @Override
            public void repaint() {
                panel.repaint();
            }

            @Override
            public boolean prochaineImage(double deltaT) {
                return animable.prochaineImage(deltaT);
            }

            @Override
            public void reinitialiser() {
                animable.reinitialiser();
            }
        };
    }

    /**
     * M\u00E9thode charg\u00E9e de repeinturer la {@link SurfaceAnimable}.
     * <br>
     * <code>repaint()</code> est automatiquement appel\u00E9 apr\u00E8s chaque
     * appel de la m\u00E9thode {@link #prochaineImage(double) prochaineImage}
     */
    void repaint();
}
