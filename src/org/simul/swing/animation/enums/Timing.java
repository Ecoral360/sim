package org.simul.swing.animation.enums;

/**
 * Enum pr\u00E9cisant les diff\u00E9rents timing possible dans la timeline
 *
 * @author Mathis Laroche
 */
public enum Timing {
    /**
     * Indique que l'animation devrait \u00EAtre jou\u00E9e en m\u00EA temps que la pr\u00E9c\u00E9dente
     */
    AVEC_PRECEDENT,
    /**
     * Indique que l'animation devrait \u00EAtre jou\u00E9e apr\u00E8s la pr\u00E9c\u00E9dente
     */
    APRES_PRECEDENT,

}
