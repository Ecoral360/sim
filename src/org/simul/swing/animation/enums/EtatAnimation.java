package org.simul.swing.animation.enums;

/**
 * Enum dans lequel sont enregistr\u00E9s les diff\u00E9rents \u00E9tats qu'une animation
 * peut avoir
 * @author Mathis Laroche
 */
public enum EtatAnimation {
    /**
     * \u00C9tat de l'animation avant qu'elle ne soit ex\u00E9cut\u00E9e
     * ou apr\u00E8s avoir \u00E9t\u00E9 recommenc\u00E9e
     */
    IDLE,
    /**
     * \u00C9tat de l'animation lorsqu'elle est en pause
     */
    PAUSE,
    /**
     * \u00C9tat de l'animation lorsqu'elle est en cours d'ex\u00E9cution
     */
    EN_COURS,
    /**
     * \u00C9tat de l'animation lorsqu'elle a fini son ex\u00E9cution
     */
    FINI
}
