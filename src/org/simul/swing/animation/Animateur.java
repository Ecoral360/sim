package org.simul.swing.animation;

import org.jetbrains.annotations.NotNull;
import org.simul.swing.animation.enums.EtatAnimation;
import org.simul.swing.animation.interfaces.Animable;
import org.simul.swing.animation.interfaces.SurfaceAnimable;

/**
 * Classe responsable d'animer des objets {@link Animable}
 *
 * @author Mathis Laroche
 */
public class Animateur {
    private final boolean faireEnBoucle;
    private final SurfaceAnimable animable;
    /**
     * image par seconde
     */
    private int sleep, tempsReelEcoule = 0;
    private double deltaT;
    private int nbImageEntreRepaint = 0;
    private int currentImage = 0;
    private double tempsEcoule = 0;

    private volatile EtatAnimation etatAnimation = EtatAnimation.IDLE;

    public Animateur(@NotNull SurfaceAnimable animable, int sleep, double deltaT, boolean faireEnBoucle) {
        this.animable = animable;
        this.faireEnBoucle = faireEnBoucle;
        this.sleep = sleep;
        this.deltaT = deltaT;
    }

    public Animateur(@NotNull SurfaceAnimable animable, int sleep, boolean faireEnBoucle) {
        this(animable, sleep, 1.0 / sleep, faireEnBoucle);
    }

    public Animateur(@NotNull SurfaceAnimable animable, int sleep, double deltaT) {
        this(animable, sleep, deltaT, false);
    }

    public Animateur(@NotNull SurfaceAnimable animable, int sleep) {
        this(animable, sleep, false);
    }

    public Animateur(@NotNull SurfaceAnimable animable, double deltaT) {
        this(animable, (int) (1 / deltaT), deltaT, false);
    }

    /**
     * Ouvre un {@link Thread} dans lequel sera ex\u00E9cut\u00E9 l'animation. <br>
     * Si l'animation est d\u00E9j\u00e0 en cours ou en pause, cette m\u00E9thode n'a aucune
     * effet
     */
    public void demarrer() {
        if (animable == null)
            throw new NullPointerException("L'animateur ne peut pas commencer \u00e0 animer, car la valeur d'animable est null");
        if (etatAnimation != EtatAnimation.IDLE) return;

        etatAnimation = EtatAnimation.EN_COURS;
        var thread = new Thread(this::jouerAnimation);
        thread.start();
    }

    /**
     * Pause le d\u00E9roulement de l'animation si l'animation \u00E9tait en cours<br>
     * Si l'animation est en d\u00E9j\u00e0 en pause ou si l'animation a fini d'\u00EAtre anim\u00E9e,
     * cette m\u00E9thode n'a aucun effet
     */
    public void pause() {
        if (etatAnimation != EtatAnimation.EN_COURS) return;
        etatAnimation = EtatAnimation.PAUSE;
    }

    /**
     * Reprend le d\u00E9roulement de l'animation si l'animateur \u00E9tait en pause<br>
     * Si l'animation est d\u00E9j\u00e0 en cours ou si l'animation a fini d'\u00EAtre anim\u00E9e,
     * cette m\u00E9thode n'a aucun effet
     */
    public void reprendre() {
        if (etatAnimation != EtatAnimation.PAUSE) return;
        etatAnimation = EtatAnimation.EN_COURS;
    }

    /**
     * Met l'\u00E9tat de l'animateur \u00e0 {@link EtatAnimation#FINI FINI}, ce qui a pour effet d'arr\u00EAter l'animation,
     * m\u00EAme si {@link #faireEnBoucle} est <code>true</code>
     * <br>
     * Dans cet \u00E9tat, la m\u00E9thode {@link #reprendre()} n'aura aucun effet et seul
     * la m\u00E9thode {@link #demarrer()} peut red\u00E9marrer l'animation
     */
    public void arreter() {
        if (etatAnimation == EtatAnimation.IDLE) return;
        etatAnimation = EtatAnimation.FINI;
    }

    /**
     * R\u00E9initialise l'animation dans son \u00E9tat initial<br>
     * (appel {@link Animable#reinitialiser()} sur l'objet {@link #animable})
     */
    public void reinitialiser() {
        animable.reinitialiser();
        etatAnimation = EtatAnimation.IDLE;
        tempsEcoule = 0;
        tempsReelEcoule = 0;
        animable.repaint();
    }

    /**
     * D\u00E9termine l'action logique \u00e0 effectuer selon l'\u00E9tat actuel de l'animation<br>
     * <ul>
     *     <li>
     *         {@link EtatAnimation#IDLE idle} -> {@link #demarrer()}
     *     </li>
     *     <li>
     *         {@link EtatAnimation#EN_COURS en cours} -> {@link #pause()}
     *     </li>
     *     <li>
     *         {@link EtatAnimation#PAUSE pause} -> {@link #reprendre()}
     *     </li>
     *     <li>
     *         {@link EtatAnimation#FINI fini} -> {@link #reinitialiser()}
     *     </li>
     * </ul>
     *
     * @return le nouvel \u00E9tat de l'animation
     */
    public EtatAnimation toggle() {
        switch (etatAnimation) {
            case IDLE -> demarrer();
            case EN_COURS -> pause();
            case PAUSE -> reprendre();
            case FINI -> reinitialiser();
        }
        return etatAnimation;
    }

    public void prochaineImage() {
        if (etatAnimation == EtatAnimation.FINI || etatAnimation == EtatAnimation.IDLE) return;
        tempsEcoule += deltaT;
        tempsReelEcoule += sleep;

        var estFini = !animable.prochaineImage(deltaT);
        animable.repaint();
        if (estFini) {
            if (faireEnBoucle) {
                reinitialiser();
                etatAnimation = EtatAnimation.EN_COURS;
            } else arreter();
        }
    }


    private void jouerAnimation() {
        while (etatAnimation != EtatAnimation.FINI && etatAnimation != EtatAnimation.IDLE) {
            try {
                Thread.sleep(sleep);
            } catch (InterruptedException ignore) {
            }

            if (etatAnimation == EtatAnimation.PAUSE) continue;

            tempsEcoule += deltaT;
            tempsReelEcoule += sleep;
            currentImage++;
            var estFini = !animable.prochaineImage(deltaT);
            if (nbImageEntreRepaint == 0 || (currentImage %= nbImageEntreRepaint) == 0) {
                animable.repaint();
            }
            if (estFini) {
                if (faireEnBoucle) {
                    reinitialiser();
                    etatAnimation = EtatAnimation.EN_COURS;
                } else arreter();
            }
        }
    }

    //----------------- Getters & Setters -----------------//

    /**
     * Permet d'obtenir l'\u00E9tat de l'animation
     *
     * @return l'\u00E9tat actuel de l'animation
     */
    public EtatAnimation getEtatAnimation() {
        return etatAnimation;
    }

    /**
     * <b>ATTENTION</b> l'utilisation de cette m\u00E9thode est d\u00E9conseill\u00E9e, car elle
     * peut mener \u00e0 des comportements ind\u00E9termin\u00E9s. Utilisez donc \u00e0 vos risques.<br>
     * Si vous souhaitez changer l'\u00E9tat de l'animation de fa\u00E7on s\u00E9curitaire, utilisez plut\u00F4t
     * les m\u00E9thodes {@link #reprendre()}, {@link #reinitialiser()}, {@link #pause()} ou {@link #demarrer()}
     *
     * @param etatAnimation le nouvel \u00E9tat de l'animation
     */
    public void setEtatAnimation(EtatAnimation etatAnimation) {
        this.etatAnimation = etatAnimation;
    }

    /**
     * Retourne le nombre d'image par seconde
     *
     * @return le nombre d'image par seconde
     */
    public int getSleep() {
        return sleep;
    }

    public void setSleep(int sleep) {
        this.sleep = sleep;
    }

    public double getDeltaT() {
        return deltaT;
    }

    public void setDeltaT(double deltaT) {
        this.deltaT = deltaT;
    }

    public double getTempsEcoule() {
        return tempsEcoule;
    }

    public double getTempsReelEcoule() {
        return tempsReelEcoule;
    }

    public void setNbImageEntreRepaint(int nbImageEntreRepaint) {
        this.nbImageEntreRepaint = nbImageEntreRepaint;
    }

    public void setTempsEntreRepaint(int temps) {
        this.nbImageEntreRepaint = temps / sleep;
    }

    /**
     * @return <code>true</code> si l'animation sera recommenc\u00E9e apr\u00E8s avoir jou\u00E9e
     */
    public boolean faireEnBoucle() {
        return faireEnBoucle;
    }

}
