package org.simul.swing.animation.creation;

public final class Scale extends Transformation {
    private final double scaleX, scaleY;

    public Scale(Geometrie shape, double scaleX, double scaleY, double duree, boolean smooth) {
        super(shape, duree, smooth);
        this.scaleX = scaleX;
        this.scaleY = scaleY;
    }

    public Scale(Geometrie shape, double scale, double duree, boolean smooth) {
        this(shape, scale, scale, duree, smooth);
    }

    @Override
    protected void prochaineImageNormal(double interval) {
        var step = getDuree() * 1 / interval;
        var stepX = scaleX < 1 ? -scaleX / step : scaleX / step;
        var stepY = scaleY < 1 ? -scaleY / step : scaleY / step;
        getShape().setScaleX(getShape().getScaleX() + stepX);
        getShape().setScaleY(getShape().getScaleY() + stepY);
    }

    @Override
    protected void prochaineImageSmooth(double interval) {

    }

    @Override
    public void reinitialiser() {
        super.reinitialiser();
        getShape().setScaleX(1);
        getShape().setScaleY(1);
    }
}
