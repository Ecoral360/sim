package org.simul.swing.animation.creation;

public final class Rotation extends Transformation {
    private final double angle, angleInitial;

    /**
     * @param angle en radians
     */
    public Rotation(Geometrie geometrie, double angle, double duree, boolean smooth) {
        super(geometrie, duree, smooth);
        this.angle = angle;
        this.angleInitial = geometrie.getAngle();
    }

    @Override
    protected void prochaineImageNormal(double interval) {
        var step = getDuree() * 1 / interval;
        var stepTheta = angle / step;
        getShape().setAngle(getShape().getAngle() + stepTheta);
    }

    @Override
    protected void prochaineImageSmooth(double interval) {

    }

    @Override
    public void reinitialiser() {
        super.reinitialiser();
        getShape().setAngle(angleInitial);
    }

    //----------------- Getters && Setters -----------------//

    public double getAngle() {
        return angle;
    }
}
