package org.simul.swing.animation.creation;

import org.simul.swing.dessin.Dessinable;

/**
 * Classe abstraite poss\u00E9dant les m\u00E9thodes \u00e0 impl\u00E9menter pour cr\u00E9er
 * une figure g\u00E9om\u00E9trique
 */
public abstract class Geometrie implements Dessinable {
    private double
            x,
            y,
            angle = 0.0,
            scaleX = 1,
            scaleY = 1;

    protected Geometrie(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public abstract void creerGeometrie();

    //----------------- Getters && Setters -----------------//

    public double getX() {
        return this.x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return this.y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getScaleX() {
        return scaleX;
    }

    public void setScaleX(double scaleX) {
        this.scaleX = scaleX;
    }

    public double getScaleY() {
        return scaleY;
    }

    public void setScaleY(double scaleY) {
        this.scaleY = scaleY;
    }

    public double getAngle() {
        return this.angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

}
