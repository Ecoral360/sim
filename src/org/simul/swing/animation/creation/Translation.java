package org.simul.swing.animation.creation;

public final class Translation extends Transformation {
    private final double dx, dy;
    private final double xInitial, yInitial;

    /**
     * Cr\u00E9e une translation
     *
     * @param geometrie G\u00E9om\u00E9trie sur laquelle la translation est appliqu\u00E9e
     * @param dx        mouvement en x par rapport \u00e0 la position actuelle
     * @param dy        mouvement en Y par rapport \u00e0 la position actuelle
     * @param duree     le temps n\u00E9cessaire pour faire la translation (en secondes)
     * @param smooth    une animation <i>smooth</i> acc\u00E9l\u00E8rera et d\u00E9c\u00E9l\u00E8rera au d\u00E9but et \u00e0
     *                  la fin de celle-ci
     */
    public Translation(Geometrie geometrie, double dx, double dy, double duree, boolean smooth) {
        super(geometrie, duree, smooth);
        this.xInitial = geometrie.getX();
        this.yInitial = geometrie.getY();
        this.dx = dx;
        this.dy = dy;
    }

    @Override
    protected void prochaineImageNormal(double interval) {
        var step = getDuree() * 1 / interval;
        var stepX = dx / step;
        var stepY = dy / step;
        getShape().setX(getShape().getX() + stepX);
        getShape().setY(getShape().getY() + stepY);
        getShape().creerGeometrie();
    }

    @Override
    protected void prochaineImageSmooth(double interval) {

    }

    @Override
    public void reinitialiser() {
        super.reinitialiser();
        getShape().setX(xInitial);
        getShape().setY(yInitial);
        getShape().creerGeometrie();
    }

    public double getDx() {
        return dx;
    }

    public double getDy() {
        return dy;
    }

}
