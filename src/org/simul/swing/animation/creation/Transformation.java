package org.simul.swing.animation.creation;

import org.simul.swing.animation.interfaces.Animable;

public abstract class Transformation implements Animable {
    private final double duree;
    private final boolean smooth;
    private final Geometrie shape;
    private double tempsRestant;

    public Transformation(Geometrie shape, double duree, boolean smooth) {
        this.shape = shape;
        this.duree = duree;
        this.smooth = smooth;
        this.tempsRestant = duree;
    }

    /**
     * @param deltaT Interval de temps entre cette frame et la frame precedante (en secondes)<br>
     *                   <i><b>ATTENTION</b></i>: L'exactitude de cette valeur n'est <i>PAS</i> garantie!
     * @return <code>true</code> si l'animation n'est pas finie et <code>false</code> si elle est finie
     */
    public final boolean prochaineImage(double deltaT) {
        if (isSmooth())
            prochaineImageSmooth(deltaT);
        else
            prochaineImageNormal(deltaT);
        tempsRestant -= deltaT;
        return tempsRestant >= 0;
    }

    protected abstract void prochaineImageSmooth(double interval);

    protected abstract void prochaineImageNormal(double interval);

    @Override
    public void reinitialiser() {
        tempsRestant = duree;
    }

    //----------------- Getters && Setters -----------------//

    public Geometrie getShape() {
        return shape;
    }

    public boolean isSmooth() {
        return smooth;
    }

    public double getTempsRestant() {
        return tempsRestant;
    }

    public double getDuree() {
        return duree;
    }
}
