package org.simul.swing.selection;

/**
 * Interface qui definit la methode qu'un objet doit implementer pour pouvoir
 * etre selectionne
 *
 * @author Caroline Houle
 */
public interface Selectionnable {
    /**
     * Retourne vrai si le point passe en paramètre fait partie de l'objet dessinable
     * sur lequel cette methode sera appelee
     *
     * @param posX Position en x du point (en pixels)
     * @param posY Position en y du point (en pixels)
     */
    boolean contient(int posX, int posY);
}
