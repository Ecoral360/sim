package org.simul.swing.event;

import org.simul.swing.MouseEventHandler;

import java.awt.*;
import java.awt.event.*;

/**
 * @author Mathis
 */
public record ComponentEvent(Component component) {

    public static void runOnceAdded(Component component, Runnable action) {
        component.addHierarchyListener(new HierarchyListener() {
            @Override
            public void hierarchyChanged(HierarchyEvent e) {
                if (e.getComponent() == e.getChanged()) {
                    action.run();
                    component.removeHierarchyListener(this);
                }
            }
        });
    }

    public static void onDrag(Component component, MouseEventHandler on_drag) {
        component.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                on_drag.handle(e);
            }
        });
    }

    public static void onClick(Component component, MouseEventHandler on_click) {
        component.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                on_click.handle(e);
            }
        });
    }

}
