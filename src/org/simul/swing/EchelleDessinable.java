package org.simul.swing;

import org.simul.swing.dessin.Dessinable;
import org.simul.utils.unite.Echelle;

import java.awt.*;
import java.awt.geom.Line2D;

/**
 * @author Mathis Laroche
 */
public class EchelleDessinable implements Dessinable {
    public static final double HAUTEUR_BARRE = 10;
    public static final Color COULEUR = Color.white;

    private final Echelle echelle;
    private double x, y, hauteurBarre;
    private Color couleur;

    public EchelleDessinable(Echelle echelle, double x, double y, double hauteurBarre, Color couleur) {
        this.x = x;
        this.y = y;
        this.hauteurBarre = hauteurBarre;
        this.echelle = echelle;
        this.couleur = couleur;
    }

    public EchelleDessinable(Echelle echelle, double x, double y, double hauteurBarre) {
        this(echelle, x, y, hauteurBarre, COULEUR);
    }

    public EchelleDessinable(Echelle echelle, double x, double y, Color couleur) {
        this(echelle, x, y, HAUTEUR_BARRE, couleur);
    }

    public EchelleDessinable(Echelle echelle, double x, double y) {
        this(echelle, x, y, HAUTEUR_BARRE, COULEUR);
    }


    @Override
    public void dessiner(Graphics2D g2d) {
        g2d.setColor(couleur);

        final double unMetreEnPixel = echelle.unMetreEnPixels();

        Line2D.Double barreGauche = new Line2D.Double(x, y, x, y + hauteurBarre);
        Line2D.Double barreDroite = new Line2D.Double(
                x + echelle.metresEnPixels(1),
                y,
                x + echelle.metresEnPixels(1),
                y + hauteurBarre
        );
        Line2D.Double ligne = new Line2D.Double(
                x,
                y + hauteurBarre / 2,
                x + echelle.metresEnPixels(1),
                y + hauteurBarre / 2
        );
        g2d.draw(barreGauche);
        g2d.draw(barreDroite);
        g2d.draw(ligne);
        g2d.setFont(new Font("timesnewroman", Font.PLAIN, 10));
        String ratio = "%.2f pixels / %.2f metres".formatted(unMetreEnPixel, 1.0);
        var bounds = g2d.getFontMetrics().getStringBounds(ratio, g2d);
        g2d.drawString(
                "%.2f pixels / %.2f metres".formatted(unMetreEnPixel, 1.0),
                (int) (x + unMetreEnPixel / 2 - bounds.getWidth() / 2),
                (int) (y + hauteurBarre + bounds.getHeight())
        );
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getHauteurBarre() {
        return hauteurBarre;
    }

    public void setHauteurBarre(double hauteurBarre) {
        this.hauteurBarre = hauteurBarre;
    }

    public Color getCouleur() {
        return couleur;
    }

    public void setCouleur(Color couleur) {
        this.couleur = couleur;
    }

    public Echelle getEchelle() {
        return echelle;
    }
}
