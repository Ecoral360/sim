package org.simul.swing.dessin;

import org.simul.utils.unite.Echelle;

import java.awt.*;
import java.util.function.Consumer;

/**
 * Classe qui permet de dessiner de mani\u00E8re s\u00E9curitaire les objets {@link Dessinable}
 *
 * @author Mathis Laroche
 */
public record Dessinateur(Graphics2D g2d) {
    /**
     * Dessine un objet {@link Dessinable} en lui passant une <i>copie</i> du {@link Graphics2D contexte graphique}
     * afin de pr\u00E9venir un potentiel <i>leak</i> des transformations effectu\u00E9es par l'objet
     * {@link Dessinable} sur le contexte graphique
     *
     * @param dessinable objet \u00e0 dessiner
     */
    public void dessiner(Dessinable dessinable) {
        dessinable.dessiner((Graphics2D) g2d.create());
    }

    /**
     * Dessine un objet {@link Dessinable} <b>\u00C0 L'\u00C9CHELLE</b> en lui passant une <i>copie</i> du {@link Graphics2D contexte graphique}
     * afin de pr\u00E9venir un potentiel <i>leak</i> des transformations effectu\u00E9es par l'objet
     * {@link Dessinable} sur le contexte graphique
     *
     * @param dessinable objet \u00e0 dessiner
     * @param echelle    l'\u00E9chelle \u00e0 laquelle le {@link Graphics2D contexte graphique} sera ajust\u00E9e
     */
    public void dessiner(Dessinable dessinable, Echelle echelle) {
        var g2dPrive = (Graphics2D) g2d.create();
        echelle.scaleG2d(g2dPrive);
        dessinable.dessiner(g2dPrive);
    }

    public void dessiner(Dessinable dessinable, Consumer<Graphics2D> modifiers) {
        var g2dPrive = (Graphics2D) g2d.create();
        modifiers.accept(g2dPrive);
        dessinable.dessiner(g2dPrive);
    }

    public void dessinerRaw(Dessinable dessinable) {
        dessinable.dessiner(g2d);
    }

    public void dessinerRaw(Dessinable dessinable, Echelle echelle) {
        echelle.scaleG2d(g2d);
        dessinable.dessiner(g2d);
    }

    public void dessinerRaw(Dessinable dessinable, Consumer<Graphics2D> modifiers) {
        modifiers.accept(g2d);
        dessinable.dessiner(g2d);
    }
}