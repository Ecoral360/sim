package org.simul.swing.dessin;

import java.awt.*;

/**
 * Interface qui d\u00E9finit la m\u00E9thode (ou possiblement les m\u00E9thodes) qu'un objet dessinable
 * doit impl\u00E9menter.
 *
 * @author Caroline Houle
 *
 */
public interface Dessinable {
    /**
     * Dessine les formes constituant l'objet.
     * Doit s'assurer de ne pas modifier le contexte grahique
     *
     * @param g2d Contexte graphique du composant sur lequel dessiner
     */
    void dessiner(Graphics2D g2d);
}
