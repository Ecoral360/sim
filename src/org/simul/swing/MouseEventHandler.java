package org.simul.swing;

import java.awt.event.MouseEvent;

/**
 * @author Mathis Laroche
 */
public interface MouseEventHandler {
    void handle(MouseEvent e);
}
