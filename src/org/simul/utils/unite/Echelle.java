package org.simul.utils.unite;

import java.awt.*;
import java.util.Locale;

/**
 * @author Mathis Laroche
 */
public class Echelle {
    private double pixels, metres;
    private String label = "";


    public Echelle(double pixels, double metres) {
        this.pixels = pixels;
        this.metres = metres;
    }

    public double metresEnPixels(double metres) {
        return metres * ratioPixelMetre();
    }

    public double pixelsEnMetres(double pixels) {
        return pixels * ratioMetrePixel();
    }

    public double unMetreEnPixels() {
        return metresEnPixels(1);
    }

    public double unPixelEnMetres() {
        return pixelsEnMetres(1);
    }

    /**
     * @return le ratio pixels / metres
     */
    public double ratioPixelMetre() {
        return pixels / metres;
    }

    /**
     * @return le ratio metres / pixels
     */
    public double ratioMetrePixel() {
        return metres / pixels;
    }

    public void scaleG2d(Graphics2D g2d) {
        g2d.scale(ratioPixelMetre(), ratioPixelMetre());
    }

    @Override
    public String toString() {
        return String.format(
                Locale.CANADA,
                "Echelle" + (label.isBlank() ? "%s" : "(%s)") + ": %.2fpx / 1.00m",
                label,
                unMetreEnPixels()
        );
    }

    //----------------- Getters && Setters -----------------//

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setRatio(double pixels, double metres) {
        this.pixels = pixels;
        this.metres = metres;
    }

    public void setPixels(double pixels) {
        this.pixels = pixels;
    }

    public void setMetres(double metres) {
        this.metres = metres;
    }
}



















