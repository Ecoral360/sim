package org.simul.utils.annotations;

/**
 * @author Mathis Laroche
 */
public @interface Author {
    String[] value();
}
