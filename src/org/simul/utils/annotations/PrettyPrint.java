package org.simul.utils.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation utilis\u00E9e par le {@link org.simul.utils.lang.prettyprint.PrettyPrinter PrettyPrinter}
 * pour savoir les propri\u00E9t\u00E9s \u00e0 afficher
 *
 * @author Mathis Laroche
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface PrettyPrint {
    boolean includeType() default false;
}
