package org.simul.utils.annotations;

public @interface EtatAnimation {
    OptionsCopie copie() default OptionsCopie.DIRECT;

    String methodeCopieManuel() default "";

    enum OptionsCopie {
        /**
         * 
         */
        DIRECT,
        CONSTRUCTEUR,
        SERIALIZATION,
        CLONE,
        MANUEL
    }
}
