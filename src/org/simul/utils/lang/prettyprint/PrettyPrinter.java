package org.simul.utils.lang.prettyprint;

import org.simul.utils.annotations.PrettyPrint;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Mathis Laroche
 */
public final class PrettyPrinter {

    private PrettyPrinter() {
        //no instance
    }

    public static String toString(Object obj) {
        String className = obj.getClass().getSimpleName();
        //if (obj.getClass().isAnnotationPresent(PrettyPrint.class)) {
        //}

        String objToString = className + "{" +
                joinFields(
                        Stream.of(obj.getClass().getDeclaredFields())
                                .filter(field -> field.isAnnotationPresent(PrettyPrint.class)),
                        obj
                );

        return objToString + "}";
    }

    public static void println(Object obj) {
        System.out.println(toString(obj));
    }

    public static void print(Object obj) {
        System.out.print(toString(obj));
    }

    private static String joinFields(Stream<Field> fieldStream, Object obj) {
        return fieldStream.map(field -> {
                    var prettyPrintOptions = field.getAnnotation(PrettyPrint.class);
                    String fieldToString = field.getName();

                    if (prettyPrintOptions.includeType()) {
                        fieldToString += ": " + field.getType().getSimpleName() + " = ";
                    } else fieldToString += "=";

                    try {
                        field.setAccessible(true);
                        var value = field.get(obj);
                        if (value instanceof String s) {
                            fieldToString += "'" + s + "'";
                        } else if (value.getClass().isArray()) {
                            fieldToString += arrayToString(value);
                        } else {
                            fieldToString += value.toString();
                        }

                    } catch (IllegalAccessException e) {
                        fieldToString += "??";
                    }
                    return fieldToString;
                })
                .collect(Collectors.joining(", "));
    }

    private static String arrayToString(Object array) {
        var toString = new ArrayList<String>();
        for (int i = 0; i < Array.getLength(array); i++) {
            var val = Array.get(array, i);
            if (val.getClass().isArray()) {
                toString.add(arrayToString(val));
            } else {
                toString.add(val.toString());
            }
        }
        return toString.toString();
    }
}









