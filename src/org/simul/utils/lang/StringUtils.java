package org.simul.utils.lang;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

public class StringUtils {
    private StringUtils() {
        //no instance
    }

    public static <T> String join(String delimiter, Collection<T> collection) {
        return collection
                .stream()
                .map(Objects::toString)
                .collect(Collectors.joining(delimiter));
    }
}
