package org.simul.utils.lang;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class ListeUtils {
    private ListeUtils() {
        //no instance
    }

    public static <T> List<T> sublist(List<T> list, int debut) {
        return list.subList(debut, list.size());
    }

    public static <T, U> List<U> map(Collection<T> collection, Function<T, U> mappingFunction) {
        return collection.stream().map(mappingFunction).toList();
    }

    public static <T> List<T> filtrer(Collection<T> collection, Predicate<T> filtre) {
        return collection.stream().filter(filtre).toList();
    }
}
